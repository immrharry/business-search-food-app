import { useState, useEffect } from "react";
import yelp from "../api/yelp";

export default () => {
  const [results, setResults] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  const searchApi = async searchTerm => {
    try {
      const response = await yelp.get("/search", {
        params: {
          term: searchTerm,
          /*limit: 50,
          longitude: 67.01,
          latitude: 24.86*/
          location: "New York"
        }
      });
      setResults(response.data.businesses);
    } catch (e) {
      setErrorMessage("Something went wrong. Try again.");
    }
  };

  useEffect(() => {
    searchApi("food");
  }, []);

  return [searchApi, results, errorMessage];
};
