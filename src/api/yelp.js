import axios from "axios";
const apiKey =
  "uBsNGAyIv1M66CWgKUxdIITOmc7Z9oYzngUVVlVgUCaqFx-mARcKkw24hm6lecWq2jNAgU2s045rASW4o-_mTFCRcDZ0Z8732gd-hjRbayRf0u2NUHsZK6O5PCVZXnYx";

export default axios.create({
  baseURL: "https://api.yelp.com/v3/businesses",
  headers: {
    Authorization: `Bearer ${apiKey}`
  }
});
