This is a small React Native project. It is displaying businesse searches (for example, food, restaurants names etc.) by using the Yelp API by sending the GET request on:
https://api.yelp.com/v3/businesses
and displaying them nicely onto the screen by categorizing them into 'Cheap', 'Pricier' and 'Expensive'. Refer to :
https://www.yelp.com/developers/documentation/v3/business
for learning more about the API and using it into your projects.

This project was creted using expo-cli. In order to run this project on your machine, make sure you have NodeJS installed.

Clone/download the repository, run:
npm install

Then run:
expo start

This will open the Metro Bundler on browser. If you have Expo application installed on your mobile, open the Expo application and scan the QR code (Tunnel version) from the Metro Bundler browser which will open the project on your mobile device.

Note: This project was actually built using a Udemy course. This is not a unique idea. Credit goes to Stephen Grider.
